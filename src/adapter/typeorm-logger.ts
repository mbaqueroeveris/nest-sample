import { QueryRunner, Logger } from "typeorm";
import { Logger as NestjsLogger } from "@nestjs/common";

export class TypeormLogger extends NestjsLogger implements Logger {

    
    constructor(context?: string) {
        super(context);
    }

    /**
     * Logs query and parameters used in it.
     */
    logQuery(query: string, parameters?: any[], _queryRunner?: QueryRunner): any {
        super.log({query, parameters});
    }
    /**
     * Logs query that is failed.
     */
    logQueryError(error: string | Error, query: string, parameters?: any[], _queryRunner?: QueryRunner): any{
        super.error({error, query, parameters});
    }
    /**
     * Logs query that is slow.
     */
    logQuerySlow(time: number, query: string, parameters?: any[], _queryRunner?: QueryRunner): any {
        super.log({time, query, parameters});
    }
    /**
     * Logs events from the schema build process.
     */
    logSchemaBuild(message: string, _queryRunner?: QueryRunner): any {
        super.log({message});
    }
    /**
     * Logs events from the migrations run process.
     */
    logMigration(message: string, _queryRunner?: QueryRunner): any {
        super.log({message});
    }
    /**
     * Perform logging using given logger, or by default to the console.
     * Log has its own level and message.
     */
    log(level?: "log" | "info" | "warn", message?: any, queryRunner?: QueryRunner): any {
        if (level && level === 'warn') {
            super.warn({ message, queryRunner });
        } else {
            super.log(message);
        }
        
    }
}