import { Controller, Get, Post, HttpException, Body, Request, Req, Logger, UseGuards } from '@nestjs/common';
import {
  ApiBody,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';

import { UserDto } from '../../model/dto/user.dto';
import * as jwt from "jsonwebtoken";

import { UserService } from '../users/user.service';
import { SignInDto } from 'src/model/dto/sign-in.dto';
import { ChangePasswordDto } from 'src/model/dto/change-password.dto';
import { RolesGuard } from 'src/guard/roles.guard';
import { Constants } from 'src/model/constants';

@ApiTags('authorization api')
@UseGuards(RolesGuard)
@Controller('/api/auth')
export class AuthController {

  private readonly logger = new Logger(AuthController.name);

  constructor(
    private userService: UserService) { }

  @Post('/signup')
  @ApiOperation({ summary: 'Sign up' })
  @ApiBody({ type: UserDto })
  async signup(@Body() user: UserDto): Promise<object> {
    this.logger.log({ 'signup' : user });
    try {
      const value: number | any = document.getElementById("num");
      const resp = await this.userService.create(user);
      this.logger.log('created ok', resp, value);
    } catch (error) {
      this.logger.log('error', error);
      throw new HttpException(
        `Error creating user: ${error.message}`,
        400,
      );
    }

    return { message: "User registered successfully!" };
  }

  @Post('/signin')
  @ApiOperation({ summary: 'Sign in' })
  @ApiBody({ type: SignInDto })
  async signin(@Body() user: SignInDto, @Request() req): Promise<object> {
    const userFound = await this.findUser(user?.username);
    this.logger.log({ 'signin' : user });
    const passwordIsValid = this.userService.checkPassword(
      user.password,
      userFound.password
    );

    if (!passwordIsValid) {
      throw new HttpException(
        `Unauthorized: Invalid username or password`,
        401,
      );
    }

    delete userFound.password;
    const token = jwt.sign({ ...userFound }, Constants.JWT.certKey, {
      expiresIn: Constants.JWT.expiresIn, // 24 hours
    });

    if (req.session) {
      req.session.jwtsession = token;
    } else {
      throw new Error('Unable to set token in session');
    }

    return userFound;
  }

  @Post('/change-password')
  @ApiOperation({ summary: 'Change password' })
  @ApiBody({ type: SignInDto })
  async changePassword(@Body() changePassword: ChangePasswordDto): Promise<UserDto> {
    this.logger.log({ 'changePassword' : changePassword });
    let userFound = undefined;
    const userName = changePassword?.username;
    userFound = await this.findUser(userName);

    const passwordIsValid = this.userService.checkPassword(
      changePassword.oldPassword,
      userFound.password
    );
    
    if (!passwordIsValid) {
      throw new HttpException(
        `Unauthorized: Invalid username or password`,
        401,
      );
    }

    userFound.password = changePassword.newPassword;
    this.userService.update(userFound);
    return userFound;
  }

  private async findUser(userName: string): Promise<UserDto> {
    let userFound = undefined;
    try {
      userFound = await this.userService.findOne(userName);
    } catch (error) {
      this.logger.log(error);
      throw new HttpException(
        `Error searching user ${userName}`,
        500
      );
    }
    if (!userFound) {
      throw new HttpException(
        `Unauthorized: Invalid username or password`,
        401
      );
    }
    return userFound;
  }

  @Get('/me')
  @ApiOperation({ summary: 'Whoami' })
  async me(@Req() req: any): Promise<UserDto> {
    return req.user || {};
  }

  @Post('/logout')
  @ApiOperation({ summary: 'Logout' })
  async logout(@Request() req:any): Promise<boolean> {

    if (req.session) {
      delete req.session.jwtsession;
    } else {
      throw new Error('Unable to delete token from session');
    }

    return true;
  }
}
