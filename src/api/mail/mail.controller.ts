import { Controller, Get, Post, Body, Patch, Param, Delete, UseGuards, SetMetadata } from '@nestjs/common';
import { MailService } from './mail.service';
import { ApiOperation, ApiTags } from '@nestjs/swagger';

@ApiTags('mail')
@Controller('api/mail')
export class MailController {
  constructor(private readonly service: MailService) { }

  @Post('recover-password')
  @ApiOperation({ summary: 'Reset password for user' })
  recoverPassword(@Param('usermail') usermail: string) {
    return this.service.recoverPassword(usermail);
  }
}
