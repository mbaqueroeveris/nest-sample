import { MailerService } from '@nestjs-modules/mailer';
import { Injectable } from '@nestjs/common';
import { User } from 'src/model/entities/user.entity';
import { UserService } from '../users/user.service';

@Injectable()
export class MailService {
  constructor(private mailerService: MailerService, private userService: UserService) { }

  async sendUserConfirmation(usermail: string) {

    //TODO create token based on user hash information
    const token = '';
    const url = `samplemailserver.com/auth/confirm?token=${token}`;
    const users = await this.userService.find({ email: usermail });
    if (!users?.length) {
      throw new Error('User not found by email: ' + usermail);
    }
    const user = users[0];
    await this.mailerService.sendMail({
      to: usermail,
      // from: '"Support Team" <support@samplemailserver.com>', // override default from
      subject: 'Welcome to Nice App! Confirm your Email',
      template: './confirmation', // `.hbs` extension is appended automatically
      context: { // ✏️ filling curly brackets with content
        name: user.username,
        url,
      },
    });
  }

  async recoverPassword(usermail: string) {

    //TODO create token based on user hash information
    const token = '';
    const url = `samplemailserver.com/auth/confirm?token=${token}`;

    await this.mailerService.sendMail({
      to: usermail,
      // from: '"Support Team" <support@example.com>', // override default from
      subject: 'Welcome to Nice App! Confirm your Email',
      template: './confirmation', // `.hbs` extension is appended automatically
      context: { // ✏️ filling curly brackets with content
        name: usermail,
        url,
      },
    });
  }
}