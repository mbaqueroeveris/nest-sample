import { Controller, Get, Post, Body, Put, Param, Delete, UseGuards, SetMetadata } from '@nestjs/common';
import { RolesService } from './roles.service';
import { RoleDto } from 'src/model/dto/role.dto';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { RolesGuard } from 'src/guard/roles.guard';
import { Constants } from 'src/model/constants';

@ApiTags('roles')
@UseGuards(RolesGuard)
@Controller('/api/roles')
export class RolesController {
  constructor(private readonly rolesService: RolesService) { }

  @Get()
  @SetMetadata('roles', [Constants.ROLE.mantainer, Constants.ROLE.admin])
  @ApiOperation({ summary: 'Find all roles' })
  findAll(example?: RoleDto): Promise<RoleDto[]> {
    return this.rolesService.findAll(example);
  }

  @Post()
  @SetMetadata('roles', [Constants.ROLE.admin])
  @ApiOperation({ summary: 'Create role' })
  create(@Body() createRoleDto: RoleDto) {
    return this.rolesService.create(createRoleDto.name);
  }

  @Put(':id')
  @SetMetadata('roles', [Constants.ROLE.admin])
  @ApiOperation({ summary: 'Update role' })
  update(@Param('id') id: string, @Body() updateRoleDto: RoleDto) {
    return this.rolesService.update(+id, updateRoleDto);
  }

  @Delete(':id')
  @SetMetadata('roles', [Constants.ROLE.admin])
  @ApiOperation({ summary: 'Delete role' })
  remove(@Param('id') id: string) {
    return this.rolesService.remove(+id);
  }
}
