import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindOptionsWhere, Repository } from 'typeorm';
import { Role } from 'src/model/entities/role.entity';
import { RoleDto } from 'src/model/dto/role.dto';

@Injectable()
export class RolesService {

  constructor(
  @InjectRepository(Role)
  private rolesRepository: Repository<Role>) {}

  create(roleName: string):Promise<Role> {

    return this.rolesRepository.save(this.rolesRepository.create({name: roleName}));
  }

  findAll(example?: FindOptionsWhere<Role>) {
    return this.rolesRepository.findBy(example);
  }

  findOne(id: number) {
    return this.rolesRepository.findOne({ where: { id } });
  }

  update(id: number, updateRoleDto: RoleDto) {
    
    return this.rolesRepository.update(id, { name: updateRoleDto.name});
  }

  remove(id: number) {
    return this.rolesRepository.delete({ id });
  }
}
