import { Body, Controller, Get, Param, Post, Put, Delete, UseInterceptors, UseGuards, SetMetadata, Session, Req, Logger } from '@nestjs/common';
import { UserService } from './user.service';
import {
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { User } from 'src/model/entities/user.entity';
import { RolesGuard } from 'src/guard/roles.guard';
import { Constants } from 'src/model/constants';

@ApiTags('users')
@Controller('/api/users')
@UseGuards(RolesGuard)
export class UserController {

  private readonly logger = new Logger(UserController.name);

  constructor(private readonly service: UserService) { }


  @Get()
  @SetMetadata('roles', [Constants.ROLE.admin])
  @ApiOperation({ summary: 'Find users' })
  async find(@Param() user?: User): Promise<User[]> {
    this.logger.log(`find ${user}`);
    return this.service.find(user);
  }

  @Get(':username')
  @ApiResponse({
    status: 200,
    description: 'The found record',
    type: User,
  })
  async findOne(@Param('username') username: string): Promise<User> {
    return this.service.findOne(username);
  }

  @Post()
  @ApiOperation({ summary: 'Create user' })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  async create(@Body() user: User): Promise<User> {
    return this.service.create(user);
  }

  @Put()
  @SetMetadata('roles', Constants.ALL_ROLES)
  @ApiOperation({ summary: 'Update user' })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  async update(@Body() user: User): Promise<User> {
    return this.service.update(user);
  }

  @Delete()
  @ApiOperation({ summary: 'Delete user' })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  async delete(@Param('username') username: string): Promise<boolean> {
    return this.service.delete(username);
  }

}
