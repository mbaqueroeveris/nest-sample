import { Module } from '@nestjs/common';
import { UserController } from './user.controller';
import { UserService } from './user.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from '../../model/entities/user.entity';
import { Role } from '../../model/entities/role.entity';

@Module({
  imports: [
  TypeOrmModule.forFeature([User, Role])],
  controllers: [UserController],
  providers: [UserService],
  exports: [ UserService ]
})
export class UserModule {}
