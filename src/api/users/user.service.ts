import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Role } from 'src/model/entities/role.entity';
import { User } from 'src/model/entities/user.entity';
import { FindOptionsWhere, Repository } from 'typeorm';
import * as bcrypt from "bcryptjs";

@Injectable()
export class UserService {

  constructor(@InjectRepository(User)
  private usersRepository: Repository<User>,
    @InjectRepository(Role)
    private rolesRepository: Repository<Role>) { }

  findOne(username: string): Promise<User> {
    return this.usersRepository.findOne({ where: [{ username }, {email: username}], relations: ['roles'] });
  }

  find(example?: FindOptionsWhere<User>): Promise<User[]> {

    return this.usersRepository.find({ where: example, relations: ['roles'] });
  }

  async create(user: User): Promise<User> {
    delete user.id;
    user.password = bcrypt.hashSync(user.password, 8);
    user.roles = await this.rolesRepository.findBy(user.roles);
    return this.usersRepository.save(user);
  }

  async update(user: User): Promise<User> {
    const userFound = await this.usersRepository.findOneBy({ id: user.id });
    if (!userFound) {
      return null;
    }
    user.password = bcrypt.hashSync(user.password, 8);
    user.roles = await this.rolesRepository.findBy(user.roles);
    return this.usersRepository.save(user);
  }
  
  async delete(username: string): Promise<boolean> {
    const result = await this.usersRepository.delete({ username });
    if (result) {
      return true;
    }
    return false;
  }

  checkPassword(userPassword, actualPassword) {
    return bcrypt.compareSync(
      userPassword,
      actualPassword
    );
  }
}
