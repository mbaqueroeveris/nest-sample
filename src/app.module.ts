import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AuthModule } from './api/auth/auth.module';
import { UserModule } from './api/users/user.module';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './model/entities/user.entity';
import { Role } from './model/entities/role.entity';
import { CreateUsersModule } from './seeder/create-users.module';
import { RolesModule } from './api/roles/roles.module';
import { MailModule } from './api/mail/mail.module';
import { LoggerModule } from 'nestjs-pino';
import { TypeormLogger } from './adapter/typeorm-logger';


console.log(__dirname + '/../model/*.model{.ts,.js}');

@Module({
  imports: [TypeOrmModule.forRoot({
    type: 'sqlite',
    database: 'database.db',
    synchronize: true,
    logger: new TypeormLogger('database'),
    logging: true,
    entities: [User, Role],
  }),
  LoggerModule.forRoot({
    pinoHttp: {
      level: process.env.LOG_LEVEL || 'debug',
      redact: ['request.headers.authorization'],
      transport: {
        target: 'pino-pretty',
        options: {
          colorize: true,
          singleLine: true,
          levelFirst: false,
          translateTime: "yyyy-MM-dd'T'HH:mm:ss.l'Z'",
          messageFormat: "[{req.id}{req.user}][{context}] {msg}",
          ignore: "pid,hostname,context,req,res",
          errorLikeObjectKeys: ['err', 'error']
        }
      }
    }
  }),
    AuthModule, UserModule, RolesModule, CreateUsersModule, MailModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
