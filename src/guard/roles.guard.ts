import { Injectable, CanActivate, ExecutionContext, Logger } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Observable } from 'rxjs';
import * as jwt from 'jsonwebtoken';
import { RoleDto } from 'src/model/dto/role.dto';
import { Constants } from 'src/model/constants';

@Injectable()
export class RolesGuard implements CanActivate {
  private readonly logger = new Logger(RolesGuard.name);

  constructor(private reflector: Reflector) {}

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {

    const contextHandler = context.getHandler();
    const className = context.getClass();
    
    const allowedRoles = this.reflector.get<string[]>('roles', contextHandler);
    if (!allowedRoles || !allowedRoles.length) {
      return true;
    }
    const request = context.switchToHttp().getRequest();
    

    const token = request.session?.jwtsession;
    this.logger.log('token: ' + token);
    if (token) {
      jwt.verify(token, Constants.JWT.certKey, (err: any, decoded: any) => {
        this.logger.error({ 'err': err});
        this.logger.log({ 'decoded': decoded });
        if (err) {
          request.status(401).send({
            message: "Unauthorized!",
          });
        }
        request.user = decoded;
      });
    }

    const user = request?.user;
    this.logger.log(`Authorizing user ${user} to access ${className.name}.${contextHandler.name}, allowedRoles [${allowedRoles}]`);
    return this.matchRoles(allowedRoles, user?.roles);
  }
  
  matchRoles(allowedRoles: Array<string>, userRoles: Array<RoleDto> = []): boolean | Promise<boolean> | Observable<boolean> {
    this.logger.log({ 'matchRoles allowedRoles' : allowedRoles });
    for (const userRole of userRoles) {
      this.logger.log({ 'matchRoles ': userRole });
      if (allowedRoles.includes(userRole.name)) {
        this.logger.log('matchRoles true');
        return true;
      }
    }
    this.logger.log('matchRoles false');
    return false;
  }
}
