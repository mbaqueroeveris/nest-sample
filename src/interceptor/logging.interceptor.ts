import { Injectable, NestInterceptor, ExecutionContext, CallHandler, Logger } from '@nestjs/common';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable()
export class LoggingInterceptor implements NestInterceptor {

  private readonly logger = new Logger(LoggingInterceptor.name);

  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    const args = context.getArgs()
    const incomingMessage = args?.[0];
    const body = this.bodyStr(incomingMessage);
    
    JSON.stringify(incomingMessage?.body);
    const trace = `[${incomingMessage?.method}] ${incomingMessage?.url}${body}`;
    this.logger.log(`Before... ${trace}`);

    const now = Date.now();
    return next
      .handle()
      .pipe(
        tap(() => this.logger.log(`After... ${trace} (${Date.now() - now}ms)`)),
      );
  }

  private bodyStr(incomingMessage) {
    const body = incomingMessage?.body;
    if (this.hasBody(body)) {
      return `, body: ${JSON.stringify(body)}`;
    }
    return '';
  }

  private hasBody(body) {
    // This is the fastest solution
    if (body) {
      for (const _i in body) {
        return true;
      }
    }
    return false;
  }
}