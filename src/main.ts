import { NestFactory, } from '@nestjs/core';
import { AppModule } from './app.module';

import * as cookieSession from "cookie-session";
import * as cors from "cors";
import * as dotenv from "dotenv";
import { NestExpressApplication } from '@nestjs/platform-express';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { Logger } from 'nestjs-pino';
import { LoggingInterceptor } from './interceptor/logging.interceptor';

async function bootstrap() {
  dotenv.config();
  const app = await NestFactory.create<NestExpressApplication>(AppModule, { bufferLogs: true });
  app.disable("x-powered-by");
  app.use(cors());
  app.use(
    cookieSession({
      name: "jwtsession",
      keys: ["jwt" /* secret keys */],

      // Cookie Options
      maxAge: 24 * 60 * 60 * 1000, // 24 hours
    })
  );
  app.useGlobalInterceptors(new LoggingInterceptor());
  app.useLogger(app.get(Logger));
  const config = new DocumentBuilder()
    .setTitle('NTTData example')
    .setDescription('The NTTData API description')
    .setVersion('1.0')
    .addTag('api')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('swagger', app, document);
  await app.listen(3000);
}
bootstrap();
