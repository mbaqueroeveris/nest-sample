
export abstract class Constants {
  static readonly ROLE = {
    'admin': 'admin',
    'mantainer': 'mantainer',
    'user': 'user'
  };
  static readonly ALL_ROLES = Object.keys(this.ROLE);
  static readonly JWT = {
    certKey: '1234567890',
    expiresIn: 86400
  };
}

