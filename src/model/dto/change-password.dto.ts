import {
  ApiProperty,
} from '@nestjs/swagger';

export class ChangePasswordDto {

  @ApiProperty()
  declare username: string;

  @ApiProperty()
  declare oldPassword: string;

  @ApiProperty()
  declare newPassword: string;

}
