
import {
  ApiProperty,
} from '@nestjs/swagger';

export class RoleDto {

  @ApiProperty()
  declare id: number;

  @ApiProperty()
  declare name: string;
}
