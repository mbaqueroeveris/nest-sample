import {
  ApiProperty,
} from '@nestjs/swagger';

export class SignInDto {

  @ApiProperty()
  declare username: string;

  @ApiProperty()
  declare password: string;

}
