import {
  ApiProperty, getSchemaPath,
} from '@nestjs/swagger';
import { RoleDto } from './role.dto';


export class UserDto {

  @ApiProperty()
  declare id: number;

  @ApiProperty()
  declare username: string;

  @ApiProperty()
  declare email: string;

  @ApiProperty()
  declare password: string;

  @ApiProperty({
    type: 'array',
    items: { $ref: getSchemaPath(RoleDto) },
  })
  declare roles: RoleDto[];

}
