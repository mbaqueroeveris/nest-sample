import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity({name : 'role'})
export class Role {

  @PrimaryGeneratedColumn()
  declare id: number;

  @Column()
  declare name: string;
}
