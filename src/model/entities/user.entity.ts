import { Entity, Column, PrimaryGeneratedColumn, ManyToMany, JoinTable } from 'typeorm';
import { Role } from './role.entity';

@Entity({name : 'user'})
export class User {

  @PrimaryGeneratedColumn()
  declare id: number;

  @Column()
  declare username: string;

  @Column()
  declare email: string;

  @Column()
  declare password: string;

  @ManyToMany(() => Role, { cascade: true })
  @JoinTable({name: 'user_roles'})
  declare roles: Role[];

}
