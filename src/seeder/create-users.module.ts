import { Module } from '@nestjs/common';
import { APP_INTERCEPTOR } from '@nestjs/core';

import { CreateUsersService } from './create-users.service';


import { UserModule } from '../api/users/user.module';
import { RolesModule } from 'src/api/roles/roles.module';

@Module({
  imports: [UserModule, RolesModule],
  controllers: [],
  providers: [CreateUsersService],
  exports: [ ]
})
export class CreateUsersModule {}
