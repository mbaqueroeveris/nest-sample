import { Injectable, OnModuleInit } from '@nestjs/common';

import { Role } from 'src/model/entities/role.entity';
import { User } from 'src/model/entities/user.entity';

import { UserService } from 'src/api/users/user.service';
import { RolesService } from 'src/api/roles/roles.service';

@Injectable()
export class CreateUsersService implements OnModuleInit {

  constructor(private userService: UserService, private roleService: RolesService) {}
  

  async onModuleInit() {
    const roleNames = [
      'admin',
      'mantainer',
      'user'
  ];
  const createdRoles = [];

  for (const roleName of roleNames) {
    const rolesFound = await this.roleService.findAll({ name: roleName });
    if (!rolesFound.length) {
      createdRoles.push(await this.roleService.create(roleName));
    } else {
      createdRoles.push(...rolesFound);
    }
  }
    
  const users = [{
      id: null,
      username: 'admin',
      password: 'admin',
      email: 'admin@company.com',
      roles: createdRoles
  }, {
      id: null,
      username: 'mantainer',
      password: 'mantainer',
      email: 'mantainer@company.com',
      roles: createdRoles.filter(role => role.name !== 'admin')
  }, {
      id: null,
      username: 'test',
      password: 'test',
      email: 'test@company.com',
      roles: createdRoles.filter(role => role.name === 'user')
  }];

  for (const newUser of users) {
    const userFound = await this.userService.findOne(newUser.username);
    if (!userFound) {
      await this.userService.create(newUser);
    } else {
      newUser.id = userFound.id;
      await this.userService.update(newUser);
    }
  }
  }
}
